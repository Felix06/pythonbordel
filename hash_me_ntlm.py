import hashlib,binascii
import sys

argList = sys.argv

if "--help" in argList:
    print("""
    This script will hash any chain of character you input in first argument to it.
    Example of usage :
        python hash_me_ntlm.py mypassword 
        """)
    sys.exit()

if len(argList) != 2:
    print("Please enter your password in argument of this script call. Use --help for more info.")
else:
    hash = hashlib.new('md4', argList[1].encode('utf-16le')).digest()
    ntlmHash = binascii.hexlify(hash)
    properNtlmHash = str(ntlmHash).split("'")[1].upper()
    print(properNtlmHash)
